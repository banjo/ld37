﻿using UnityEngine;
using System.Collections;
using Enums;
using System;

public class SceneButton : MonoBehaviour {

	public GameObject child;
	public LayerMask RaycastMask;

	/// <summary>
	/// OnMouseUp is called when the user has released the mouse button.
	/// </summary>
	void OnMouseDown()
	{
		if(Input.GetMouseButtonDown(0))
         {
             RaycastHit hit;
             Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
             if (Physics.Raycast(ray, out hit)){
                 if( hit.collider == child.GetComponent<Collider>())
                 {
                     DoSomething();
				 }
             }
		 }
		
	}

	private void DoSomething(){
		Debug.Log("ASdD");
		Item item =  (Item)Enum.Parse(typeof(Item), this.gameObject.transform.tag);
		switch (item)
		{
			case Item.WashingMachine:
				GetComponent<Shake>().DoShake();
				break;
			// case Item.Oven:
			// 	GetComponent<ColorChange>().Change();
			// 	break;
			case Item.Cake:
				ParticleSystem[] parts = GetComponentsInChildren<ParticleSystem>();
				foreach(ParticleSystem p in parts)
					p.Play();
				break;
		}
	}
}
