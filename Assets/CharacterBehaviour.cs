﻿using UnityEngine;
// using System.Collections;
using Enums;
public class CharacterBehaviour : MonoBehaviour {

	public int characterInfo;
    public GameObject MoodObject;

	public Material mat;

	public Color newColor;
	public Color oldColor;

	private float duration = 3.0f;


	/// <summary>
	/// Start is called on the frame when a script is enabled just before
	/// any of the Update methods is called the first time.
	/// </summary>
	void Start()
	{
		mat = new Material(MoodObject.GetComponent<MeshRenderer>().sharedMaterial);
		MoodObject.GetComponent<MeshRenderer>().sharedMaterial = mat;
		oldColor = mat.color;
	}


	// Update is called once per frame
	void Update () {
		if(Vector3.Distance(Vector3.zero, this.transform.position) > 30){
            ResetPosition();
        }
		float lerp = Mathf.PingPong(Time.time, duration) / duration;
        mat.color = Color.Lerp(newColor, oldColor, lerp);

	}



	public void OnCollisionEnter(Collision other)
	{

		string tagName = other.collider.tag;
		if(tagName == "Respawn"){
			ResetPosition();
		}
		else if (other.gameObject.layer == 8){//furniture
			Character cc = CharactersController.Instance.characterQueue[characterInfo];
			if(Vector3.Angle(this.transform.up, Vector3.up) > 90f)
				cc.bonusScore += 1;
			cc.lastCollider = tagName;
		}
	}



	/// <summary>
	/// OnTriggerEnter is called when the Collider other enters the trigger.
	/// </summary>
	/// <param name="other">The other Collider involved in this collision.</param>
	void OnTriggerEnter(Collider other)
	{
		if(other.tag == Item.Trigger.ToString())
			CharactersController.Instance.characterQueue[characterInfo].bonusScore+=1;
	}

	/// <summary>
	/// OnTriggerExit is called when the Collider other has stopped touching the trigger.
	/// </summary>
	/// <param name="other">The other Collider involved in this collision.</param>
	void OnTriggerExit(Collider other)
	{
		if(other.tag == Item.Trigger.ToString())
			CharactersController.Instance.characterQueue[characterInfo].bonusScore-=1;
	}


	void ResetPosition(){
		this.transform.position = Vector3.up*10;
		Rigidbody[] rigids = GetComponentsInChildren<Rigidbody>();
		foreach(Rigidbody r in rigids){
			r.velocity = Vector3.zero;
			r.angularVelocity = Vector3.zero;
		}
		Debug.Log("RESET");
	}
}
