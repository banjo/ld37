﻿using UnityEngine;
using UnityEngine.UI;
// using System.Collections;
using System.Collections.Generic;
using System.Linq;
public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance = null;
    public Level currentLevel;
    public Text timerText;
    public Color defaultColor;



    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        // Setup everything for currentlevel
        currentLevel = new Level(10, 120f);
    }

    // Use this for initialization
    void Start()
    {
        timerText.text = "";
    }

    // Update is called once per frame
    void Update()
    {
        // Timer Stuff
        currentLevel.TimeRemaining -= Time.unscaledDeltaTime;
        if (currentLevel.TimeRemaining > 1f)
        {
            // Set Minutes and seconds
            float minutes = Mathf.Floor(currentLevel.TimeRemaining / 60);
            float seconds = Mathf.Floor(currentLevel.TimeRemaining % 60);
            // Set default color
            Color timerColor = defaultColor;
            if (minutes == 0)
            {
                if (seconds < 21 && seconds > 10)
                {
                    // Green
                    timerColor = new Color(0.416f, 0.918f, 0.608f, 1.0f);
                }
                else if (seconds < 11 && seconds > 5)
                {
                    // Yellow
                    timerColor = new Color(0.969f, 0.839f, 0.298f, 1.0f);
                }
                else if (seconds < 6 && seconds > 0)
                {
                    // Red
                    timerColor = new Color(0.91f, 0.38f, 0.38f, 1.0f);
                }
            }
            timerText.color = timerColor;
            timerText.text = string.Format("{0}:{1}",
                                            minutes.ToString("00"),
                                            seconds.ToString("00"));
        }
        else if (currentLevel.TimeRemaining < 1f)
        {
            timerText.color = new Color(0.91f, 0.38f, 0.38f, 1.0f);
            timerText.text = "Time's Up!";
            CharactersController.Instance.CardBoardContainer.SetActive(false);
            CharactersController.Instance.LetsHaveAParty();
            Time.timeScale = 0f;
            

            if (currentLevel.TimeRemaining < -1f)
            {
                EventManager.LoadScene(EventManager.Scenes.Final);
            }else{
                Judging();
            }

        }
    }



    public void Judging()
    {
        List<Character> chars = CharactersController.Instance.characterQueue.ToList();
        string results = "";
        int sum = 0;
        foreach (Character ch in chars)
        {
            int chScore = 0;
            int bonus = ch.bonusScore;
            foreach(Score sc in ch.scores){
                if(sc.item.ToString().Equals(ch.lastCollider)){
                    chScore+=(sc.score+(int)ch.mood);
                    break;
                }
            }
            chScore *= ch.bonusScore;
            results += ch.name + " scores: " + chScore + " stays on "+ ch.lastCollider + " mood: "+ ch.mood +"\n";
            sum+=chScore;
        }
        results+= "Final Score: " +  sum;
        PlayerPrefs.SetString("Results", "Final Score: " +  sum);

    }
}
