﻿using UnityEngine;

public class KeyboardController : MonoBehaviour {
	
	// Update is called once per frame
	void Update () {
		if ((Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A)) && !RoomManager.Instance.rotating){
            StartCoroutine(RoomManager.Instance.RotateRoom(-1));
        }else if ((Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D)) && !RoomManager.Instance.rotating){
            StartCoroutine(RoomManager.Instance.RotateRoom(1));
        }
	}
}