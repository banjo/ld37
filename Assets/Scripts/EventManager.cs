﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class EventManager : MonoBehaviour {

    public enum Scenes{
        Entry,
        Game,
        Final
    }

    public void StartOver(){
        if(CharactersController.Instance != null)
            Destroy(CharactersController.Instance.gameObject);
        SceneManager.LoadScene(0);
    }

    /// <summary>
    /// Load Game Scene
    /// </summary>
	public void NewGame(){
        if(CharactersController.Instance != null)
            Destroy(CharactersController.Instance.gameObject);
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    public static void LoadScene(Scenes scene){
        if(scene == Scenes.Final){
            DontDestroyOnLoad(CharactersController.Instance.gameObject);
        }
        SceneManager.LoadScene((int) scene);
    }

    

    /// <summary>
    /// Rotate Room
    /// </summary>
    /// <param name="direction">0-left 1-right</param>
    public void RotateRoom(int direction){
        // StopAllCoroutines();
        if (!RoomManager.Instance.rotating)
            StartCoroutine(RoomManager.Instance.RotateRoom(direction));
    }


}
