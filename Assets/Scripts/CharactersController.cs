﻿using System.Collections.Generic;
using UnityEngine;
using ExtensionMethods;
using UnityEngine.UI;
using Enums;
public class CharactersController : MonoBehaviour
{
    [SerializeField]
    public List<Character> availableChars;

    public GameObject CardBoardContainer;

    public List<Character> characterQueue;

    public GameObject characterUICard;

    public static CharactersController Instance = null;

    private float cardWidth;

    private float cardOffset;



    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Destroy(this.gameObject);
            return;
        }

        Instance = this;
        DontDestroyOnLoad(gameObject);

        cardWidth = Screen.width / 12;
        cardOffset = cardWidth / 4;
    }

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {

        characterQueue = new List<Character>();
        MakeQueue();
    }

    
	public void LetsHaveAParty(){
		foreach(Transform child in this.transform){
			child.transform.GetChild(0).gameObject.SetActive(true);
		}
	}

    public void MakeQueue()
    {

        // CardBoardContainer.GetComponent<RectTransform>().sizeDelta = new Vector2(0, 5*250f);

        LevelManager.Instance.currentLevel.MaxGuestCount.Times(i =>
        {
            Character c = new Character(availableChars.RandomItem());
            characterQueue.Add(c);
            AddCardsToCardBoard(i, c.cardImage);
        });
        CardBoardContainer.GetComponent<RectTransform>().offsetMax = Vector2.zero;
        CardBoardContainer.GetComponent<RectTransform>().offsetMin = new Vector2(0, cardWidth - LevelManager.Instance.currentLevel.MaxGuestCount * (cardWidth + cardOffset));

    }

    private void AddCardsToCardBoard(int i, Sprite image)
    {
        GameObject cardClone = Instantiate(characterUICard);
        cardClone.GetComponent<CardObject>().charIndex = i;
        cardClone.transform.GetChild(0).GetComponent<Image>().sprite = image;
        cardClone.GetComponent<CanvasGroup>().blocksRaycasts = i == LevelManager.Instance.currentLevel.MaxGuestCount - 1;
        cardClone.transform.SetParent(CardBoardContainer.transform);
        cardClone.GetComponent<RectTransform>().sizeDelta = Vector2.one * cardWidth;
        cardClone.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, (i + 1) * (cardWidth + cardOffset));

    }


    public void UpdateCardBoard()
    {
        if (CardBoardContainer.transform.childCount > 0)
        {
            CardBoardContainer.GetComponent<RectTransform>().offsetMax = Vector2.zero;
            CardBoardContainer.GetComponent<RectTransform>().offsetMin += new Vector2(0, cardWidth + cardOffset);

            CardBoardContainer.transform.GetChild(CardBoardContainer.transform.childCount - 1).GetComponent<CanvasGroup>().blocksRaycasts = true;
        }

    }

    public void SpawnCharacter(int queueIndex, Vector3 position)
    {
        Character ch = characterQueue[queueIndex];

        GameObject characterGameObject = Instantiate(ch.prefab, position + Vector3.up, Quaternion.Euler(0, 180f, 0)) as GameObject;
        characterGameObject.GetComponent<CharacterBehaviour>().newColor += ch.setMoodColor();
        ch.Active = true;
        ch.ActiveGameObject = characterGameObject;
        characterGameObject.GetComponent<CharacterBehaviour>().characterInfo = queueIndex;
        characterGameObject.transform.SetParent(this.transform);
        UpdateCardBoard();
    }
}
