using UnityEngine;
using System.Collections.Generic;

namespace ExtensionMethods
{
    public static class Vector2Extensions {

        public static Vector2 xy(this Vector3 v) {
            return new Vector2(v.x, v.y);
        }

        public static Vector2 WithX(this Vector2 v, float x) {
            return new Vector2(x, v.y);
        }
        
        public static Vector2 WithY(this Vector2 v, float y) {
            return new Vector2(v.x, y);
        }

        /// <summary>
		/// Finds the position closest to the given one.
		/// </summary>
		/// <param name="position">World position.</param>
		/// <param name="otherPositions">Other world positions.</param>
		/// <returns>Closest position.</returns>
		public static Vector2 GetClosest (this Vector2 position, IEnumerable<Vector2> otherPositions)
		{
			var closest = Vector2.zero;
			var shortestDistance = Mathf.Infinity;

			foreach (var otherPosition in otherPositions) {
				var distance = (position - otherPosition).sqrMagnitude;

				if (distance < shortestDistance) {
					closest = otherPosition;
					shortestDistance = distance;
				}
			}

			return closest;
		}

    }
}

