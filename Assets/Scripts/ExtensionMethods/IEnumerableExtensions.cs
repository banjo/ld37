using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace ExtensionMethods
{
    public static class IEnumerableExtensions {
        /// <summary>
        /// Iterates over each element in the IEnumerable, passing in the element to the provided callback.
        /// </summary>
        public static void Each<T>(this IEnumerable<T> iterable, Action<T> callback) {
            foreach(var value in iterable) {
                callback(value);
            }
        }

        /// <summary>
        /// Iterates over each element in the IEnumerable, passing in the element to the provided callback.  Since the IEnumerable is
        /// not generic, a type must be specified as a type parameter to Each.
        /// </summary>
        /// <description>
        /// IEnumerable myCollection = new List<int>();
        /// ...
        /// myCollection.Each<int>(i => Debug.Log("i: " + i));
        /// </description>
        public static void Each<T>(this IEnumerable iterable, Action<T> callback) {
            foreach(T value in iterable) {
                callback(value);
            }
        }

        /// <summary>
        /// Iterates over each element in the IEnumerable, passing in the element and the index to the provided callback.
        /// </summary>
        public static void EachWithIndex<T>(this IEnumerable<T> iterable, Action<T, int> callback) {
            var i = 0;
            foreach(var value in iterable) {
                callback(value, i);
                ++i;
            }
        }

        /// <summary>
        /// Iterates over each element in the IEnumerable, passing in the element and the index to the provided callback.
        /// </summary>
        public static void EachWithIndex<T>(this IEnumerable iterable, Action<T, int> callback) {
            var i = 0;
            foreach(T value in iterable) {
                callback(value, i);
                ++i;
            }
        }

        /// <summary>
        /// Iterates over each element in the two dimensional array, passing in the index to the provided callback.
        /// </summary>
        public static void EachIndex<T>(this IEnumerable<T> iterable, Action<int> callback) {
            var i = 0;
            #pragma warning disable 0168
            foreach(var value in iterable) {
                #pragma warning restore 0168
                callback(i);
                ++i;
            }
        }

        /// <summary>
        /// Iterates over each element in the two dimensional array, passing in the index to the provided callback.
        /// </summary>
        public static void EachIndex<T>(this IEnumerable iterable, Action<int> callback) {
            var i = 0;
            #pragma warning disable 0219
            foreach(T value in iterable) {
                #pragma warning restore 0219
                callback(i);
                ++i;
            }
        }

        /// <summary>
        /// Iterates over each element in both the iterable1 and iterable2 collections, passing in the current element of each collection into the provided callback.
        /// Stops as soon as either enumerable reaches its end.
        /// </summary>
        public static void Zip<T, U>(this IEnumerable<T> iterable1, IEnumerable<U> iterable2, Action<T, U> callback) {
            var i1Enumerator = iterable1.GetEnumerator();
            var i2Enumerator = iterable2.GetEnumerator();

            while(i1Enumerator.MoveNext() && i2Enumerator.MoveNext()) {
                callback(i1Enumerator.Current, i2Enumerator.Current);
            }
        }

        /// <summary>
        /// Iterates over each element in both the iterable1 and iterable2 collections, passing in the current element of each collection into the provided callback.
        /// Stops as soon as either enumerable reaches its end.
        /// </summary>
        public static void Zip(this IEnumerable iterable1, IEnumerable iterable2, Action<object, object> callback) {
            var i1Enumerator = iterable1.GetEnumerator();
            var i2Enumerator = iterable2.GetEnumerator();

            while(i1Enumerator.MoveNext() && i2Enumerator.MoveNext()) {
                callback(i1Enumerator.Current, i2Enumerator.Current);
            }
        }

        /// <summary>
        /// Iterates over each element in both the iterable1 and iterable2 collections, passing in the current element of each collection into the provided callback.
        /// </summary>
        public static void InParallelWith<T, U>(this IEnumerable<T> iterable1, IEnumerable<U> iterable2, Action<T, U> callback) {
            if(iterable1.Count() != iterable2.Count()) throw new ArgumentException(string.Format("Both IEnumerables must be the same length, iterable1: {0}, iterable2: {1}", iterable1.Count(), iterable2.Count()));

            var i1Enumerator = iterable1.GetEnumerator();
            var i2Enumerator = iterable2.GetEnumerator();

            while(i1Enumerator.MoveNext()) {
                i2Enumerator.MoveNext();
                callback(i1Enumerator.Current, i2Enumerator.Current);
            }
        }

        /// <summary>
        /// Iterates over each element in both the iterable1 and iterable2 collections, passing in the current element of each collection into the provided callback.
        /// </summary>
        public static void InParallelWith(this IEnumerable iterable1, IEnumerable iterable2, Action<object, object> callback) {
            var i1Enumerator = iterable1.GetEnumerator();
            var i2Enumerator = iterable2.GetEnumerator();
            var i1Count = 0;
            var i2Count = 0;
            while(i1Enumerator.MoveNext()) ++i1Count;
            while(i2Enumerator.MoveNext()) ++i2Count;
            if(i1Count != i2Count) throw new ArgumentException(string.Format("Both IEnumerables must be the same length, iterable1: {0}, iterable2: {1}", i1Count, i2Count));

            i1Enumerator.Reset();
            i2Enumerator.Reset();
            while(i1Enumerator.MoveNext()) {
                i2Enumerator.MoveNext();
                callback(i1Enumerator.Current, i2Enumerator.Current);
            }
        }

        public static bool IsEmpty<T>(this IEnumerable<T> iterable) {
            return iterable.Count() == 0;
        }

        public static bool IsEmpty(this IEnumerable iterable) {
            // MoveNext returns false if we are at the end of the collection
            return !iterable.GetEnumerator().MoveNext();
        }

        public static bool IsNotEmpty<T>(this IEnumerable<T> iterable) {
            return iterable.Count() > 0;
        }

        public static bool IsNotEmpty(this IEnumerable iterable) {
            // MoveNext returns false if we are at the end of the collection
            return iterable.GetEnumerator().MoveNext();
        }

        /// <summary>
        /// Matches all elements where the given condition is not true. This is the
        /// opposite of Linq's Where clause.
        /// </summary>
        public static IEnumerable<T> ExceptWhere<T>(this IEnumerable<T> iterable, Func<T, bool> condition) {
            return iterable.Where(element => !condition(element));
        }
    }
}

