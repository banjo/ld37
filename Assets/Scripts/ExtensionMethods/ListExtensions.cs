using System;
using System.Collections.Generic;

namespace ExtensionMethods
{
    public static class ListExtensions
    {

        /// <summary>
        /// Shuffle the list in place using the Fisher-Yates method.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        public static void Shuffle<T>(this IList<T> list)
        {
            System.Random rng = new System.Random();
            int n = list.Count;
            while (n > 1)
            {
                n--;
                int k = rng.Next(n + 1);
                T value = list[k];
                list[k] = list[n];
                list[n] = value;
            }
        }

        /// <summary>
        /// Return a random item from the list.
        /// Sampling with replacement.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public static T RandomItem<T>(this List<T> list)
        {
            if (list.Count == 0) throw new System.IndexOutOfRangeException("Cannot select a random item from an empty list");
            return list[UnityEngine.Random.Range(0, list.Count)];
        }

        /// <summary>
        /// Removes a random item from the list, returning that item.
        /// Sampling without replacement.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list"></param>
        /// <returns></returns>
        public static T RemoveRandom<T>(this IList<T> list)
        {
            if (list.Count == 0) throw new System.IndexOutOfRangeException("Cannot remove a random item from an empty list");
            int index = UnityEngine.Random.Range(0, list.Count);
            T item = list[index];
            list.RemoveAt(index);
            return item;
        }

        /// <summary>
        /// Returns the element before the given element. This can wrap. If the element is the only one in the list, itself is returned.
        /// </summary>
        public static T ElementBefore<T>(this IList<T> list, T element, bool wrap = true) {
            var targetIndex = list.IndexOf(element) - 1;
            if (wrap) {
                return targetIndex < 0 ? list[list.Count - 1] : list[targetIndex];
            }
            return list[targetIndex];
        }

        [ThreadStatic]
        static System.Random randomNumberGenerator = new System.Random(DateTime.Now.Millisecond + System.Threading.Thread.CurrentThread.GetHashCode());

        /// <summary>
        /// Returns a sub-section of the current list, starting at the specified index and continuing to the end of the list.
        /// </summary>
        public static List<T> FromIndexToEnd<T>(this List<T> list, int start) {
            return list.GetRange(start, list.Count - start);
        }

        /// <summary>
        /// Returns the first index in the IList<T> where the target exists.  If the target cannot be found, returns -1.
        /// </summary>
        public static int IndexOf<T>(this IList<T> list, T target) {
            for(var i = 0; i < list.Count; ++i) {
                if(list[i].Equals(target)) return i;
            }
            return -1;
        }

        // /// Returns a randomly selected item from IList<T> determined by a IEnumerable<float> of weights
        // public static T RandomElement<T>(this IList<T> list, IEnumerable<float> weights) {
        //     if(list.IsEmpty()) throw new IndexOutOfRangeException("Cannot retrieve a random value from an empty list");
        //     if(list.Count != weights.Count()) throw new IndexOutOfRangeException("List of weights must be the same size as input list");

        //     var randomWeight = randomNumberGenerator.NextDouble() * weights.Sum();
        //     var totalWeight = 0f;
        //     var index = 0;
        //     foreach(var weight in weights) {
        //         totalWeight += weight;
        //         if (randomWeight <= totalWeight) {
        //             break;
        //         }
        //     }

        //     return list[index];
        // }

        public static IList<T> InPlaceShuffle<T>(this IList<T> list) {
            // OrderBy and Sort are both broken for AOT compliation on older MonoTouch versions
            // https://bugzilla.xamarin.com/show_bug.cgi?id=2155#c11

            for (var i = 0; i < list.Count; ++i) {
                var temp = list[i];
                var swapIndex = randomNumberGenerator.Next(list.Count);
                list[i] = list[swapIndex];
                list[swapIndex] = temp;
            }
            return list;
        }

        public static IList<T> InPlaceOrderBy<T, TKey>(this IList<T> list, Func<T, TKey> elementToSortValue) where TKey : IComparable {
            // Provides both and in-place sort as well as an AOT on iOS friendly replacement for OrderBy
            if (list.Count < 2) {
                return list;
            }

            int startIndex;
            int currentIndex;
            int smallestIndex;
            T temp;

            for (startIndex = 0; startIndex < list.Count; ++startIndex) {
                smallestIndex = startIndex;
                for (currentIndex = startIndex + 1; currentIndex < list.Count; ++currentIndex) {
                    if (elementToSortValue(list[currentIndex]).CompareTo(elementToSortValue(list[smallestIndex])) < 0) {
                        smallestIndex = currentIndex;
                    }
                }
                temp = list[startIndex];
                list[startIndex] = list[smallestIndex];
                list[smallestIndex] = temp;
            }

            return list;
        }

        /// <summary>
        /// Attempts to Insert the item, but Adds it if the index is invalid.
        /// </summary>
        public static void InsertOrAdd<T>(this IList<T> list, int atIndex, T item) {
            if (atIndex >= 0 && atIndex < list.Count) {
                list.Insert(atIndex, item);
            } else {
                list.Add(item);
            }
        }

        /// <summary>
        /// Returns the element after the given element. This can wrap. If the element is the only one in the list, itself is returned.
        /// </summary>
        public static T ElementAfter<T>(this IList<T> list, T element, bool wrap = true) {
            var targetIndex = list.IndexOf(element) + 1;
            if (wrap) {
                return targetIndex >= list.Count ? list[0] : list[targetIndex];
            }
            return list[targetIndex];
        }
    }
}

