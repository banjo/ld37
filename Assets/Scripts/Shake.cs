 using UnityEngine;
 using System.Collections;

 public class Shake : MonoBehaviour {
     
 
	public bool Shaking; 
	private float ShakeDecay;
	public float ShakeIntensity;    
	private Vector3 OriginalPos;
	private Quaternion OriginalRot;
	
	void Start()
	{
		Shaking = false;   
	}
	
	IEnumerator UpdateCoroutine () 
	{
		while(Shaking){
			if(ShakeIntensity > 0)
			{
				transform.position = OriginalPos + Random.insideUnitSphere * ShakeIntensity;
				transform.rotation = new Quaternion(OriginalRot.x + Random.Range(-ShakeIntensity, ShakeIntensity)*.2f,
										OriginalRot.y + Random.Range(-ShakeIntensity, ShakeIntensity)*.2f,
										OriginalRot.z + Random.Range(-ShakeIntensity, ShakeIntensity)*.2f,
										OriginalRot.w + Random.Range(-ShakeIntensity, ShakeIntensity)*.2f);
		
				ShakeIntensity -= ShakeDecay;
			}
			else if (Shaking)
			{
				Shaking = false;  
			}
			yield return null;
		}
	}

	public void DoShake()
	{
		OriginalPos = transform.position;
		OriginalRot = transform.rotation;
	
		if(ShakeIntensity != -1)// DONT SHAKE
			ShakeIntensity = 0.07f;
		ShakeDecay = 0.0002f;
		Shaking = true;
		StartCoroutine(UpdateCoroutine());
	}    
 }