﻿/// <summary>
/// Level class to represent level info
/// </summary>
public class Level {
	// Max number of guests to get on the party
	public int MaxGuestCount { get; set; }

	// Max number of seconds to give player to fill the room
	public float TimeRemaining { get; set; }

	// Current number of guests received and put
	public int CurrentGuestCount { get; set; }

	// Max Score that can be achieved in level
	public int MaxScore { get; set; }

	// Score needed to beat the game
	public int ScoreNeeded { get; set; }

    // Level final Score
    public int FinalScore { get; set; }

    public Level(int MaxGuestCount, float MaxSeconds){
        this.MaxGuestCount = MaxGuestCount;
        this.TimeRemaining = MaxSeconds;
        this.CurrentGuestCount = 0;
        this.MaxScore = MaxGuestCount * 10;
        this.FinalScore = 0;
        this.ScoreNeeded = this.MaxScore / 2;
    }
}
