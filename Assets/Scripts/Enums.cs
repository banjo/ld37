namespace Enums
{
    public enum CharacterType
    {
        Man = 0,
        Woman,
        Boy,
        Girl,
        Doggo,
        Toddler,
        Pigge,
        _typeCount
    }

    public enum CharacterMood
    {
        Bad=-2,
        Good=0,
        Excelent=2,
        _typeCount
    }

    public enum Item
    {
        Bed,
        Table,
        Chair,
        Cake,
        Dresser,
        Shelf,
        Fridge,
        TV,
        WashingMachine,
        Oven,
        PartyHat,
        Sofa,
        Picture,
        Wall,
        Pillow,
        Trigger
    }

}