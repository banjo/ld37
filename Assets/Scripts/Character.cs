using UnityEngine;
using Enums;
using System.Collections.Generic;


[System.SerializableAttribute]
public struct Score
{
    public Item item;
    public int score;
}


[SerializeField]

[System.SerializableAttribute]
public class Character
{
    public string name;
    public GameObject prefab;
    public GameObject ActiveGameObject;
    public CharacterType type;
    public Sprite cardImage;
    public bool Active;
    public string lastCollider;
    public CharacterMood mood;

    public List<Score> scores = new List<Score>();
    public int bonusScore = 1;


    public Character(GameObject prefab, string name, CharacterType type)
    {
        this.prefab = prefab;
        this.name = name;
        this.type = type;
    }

    public Character(Character other)
    {
        this.prefab = other.prefab;
        this.name = other.name;
        this.type = other.type;
        this.cardImage = other.cardImage;
        this.Active = false;
        this.scores = other.scores;
        
        int m = Random.Range((int)CharacterMood.Bad, (int)CharacterMood._typeCount);
        if(m%2!=0)m=(m+1)%(int)CharacterMood._typeCount;
        this.mood = (CharacterMood) m;
    }

    public Color setMoodColor(){
        Color c = Color.white;
        switch(mood){
            case CharacterMood.Excelent:
                c = Color.blue;
                break;
            case CharacterMood.Good:
                c = Color.black;
                break;
            case CharacterMood.Bad:
                c = Color.red;
                break;
        }
        return c;
    }
}