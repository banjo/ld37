﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;

public class RoomManager : MonoBehaviour {

	public GameObject WallsGameObject;
	public Camera roomCamera;

	private Transform RotationPoint;

	private List<Wall> walls;
	private GameObject RoomGameObject;

	private Transform ScaleDownTransform;
	private Transform ScaleUpTransform;

	// Invisible Wall indices;
	public int leftHidden, rightHidden;

	public bool rotating;

	public float wallSize;
	public float wallHeight;

	//coroutines :<<<
	bool[] ended = new bool[3];

	public class Wall{
		public GameObject wall;
		public bool visable;
		// public int index;
		
		public Wall(GameObject w){
			wall = w;
			visable = true;
		}
	}


	public static RoomManager Instance = null;

	/// <summary>
	/// Awake is called when the script instance is being loaded.
	/// </summary>
	void Awake()
	{
		if(Instance == null){
			Instance = this;
		}else if(Instance != this){
			Destroy(gameObject);
		}

		RoomGameObject = this.gameObject;
        rotating = false;
    }


	// Use this for initialization
	void Start () {
		walls = new List<Wall>();
		for (int i = 0; i < 5; i++)
		{
			walls.Add(new Wall(WallsGameObject.transform.GetChild(i).gameObject));
		}
		leftHidden = 1;
		rightHidden = (leftHidden+1) % 5;

		walls[leftHidden].visable = false;
		walls[rightHidden].visable = false;

		ScaleDownTransform = walls[leftHidden].wall.transform;
		ScaleUpTransform = walls[rightHidden].wall.transform;

		ScaleDownTransform.position = new Vector3(ScaleDownTransform.position.x, 0f, ScaleDownTransform.position.z);
		ScaleDownTransform.localScale = new Vector3(wallSize, 0.1f, 1f);

		ScaleUpTransform.position = new Vector3(ScaleUpTransform.position.x, 0f, ScaleUpTransform.position.z);
		ScaleUpTransform.localScale = ScaleDownTransform.localScale;

		RotationPoint = walls[0].wall.transform;
		// roomCamera.transform.LookAt(RotationPoint.position);
		// roomCamera.transform.Rotate(new Vector3(-20f, 0, 0));

	}
	
    /// <summary>
    /// Rotate Room
    /// </summary>
    /// <param name="direction"> -1 - left 1 - right</param>
	public IEnumerator RotateRoom(int dir){

        rotating = true;

        int scaleUp, scaleDown;

		if(dir == -1){
			scaleUp = rightHidden;

			rightHidden = leftHidden;
			leftHidden = leftHidden == 1 ? 4 : leftHidden-1;

			scaleDown = leftHidden;

		}else{
			scaleUp = leftHidden;

			leftHidden = rightHidden;
			rightHidden = rightHidden == 4 ? 1 : rightHidden+1;

			scaleDown = rightHidden;
		}

		Quaternion oldRot = roomCamera.transform.rotation;
		ScaleDownTransform = walls[scaleDown].wall.transform;
		ScaleUpTransform = walls[scaleUp].wall.transform;

		ended = new bool[3];
		

		StartCoroutine(RotateCamera(roomCamera.transform, dir, 0));
		StartCoroutine(ScaleWalls(ScaleDownTransform, 0f, 0.1f, 1));
		StartCoroutine(ScaleWalls(ScaleUpTransform, wallHeight/2, wallHeight, 2));

		while(!ended[0] || !ended[1] || !ended[2]){
			yield return null;
		}
        rotating = false;
		yield return null;
	}

	private IEnumerator RotateCamera(Transform cam, int dir, int ind){
		float rotateTime = 0.5f;
		float step = 0; //non-smoothed
    	float rate = 1/rotateTime; //amount to increase non-smooth step by
    	float smoothStep = 0; //smooth step this time
    	float lastStep = 0; //smooth step last time
    	while(step < 1.0) { // until we're done
			step += Time.deltaTime * rate; //increase the step
			smoothStep = Mathf.SmoothStep(0, 1, step); //get the smooth step
			cam.RotateAround(RotationPoint.position, Vector3.down, 
								dir*90f * (smoothStep - lastStep));

			lastStep = smoothStep; //store the smooth step
			yield return null;
    	}
		ended[ind] = true;
	}


	private IEnumerator ScaleWalls(Transform wall, float toPos, float toScale, int ind){

		float fromPos = wall.transform.position.y;
		float fromScale = wall.transform.localScale.y;

		float maxPos = Mathf.Max(fromPos, toPos);
		float minPos = Mathf.Min(fromPos, toPos);

		float maxScale = Mathf.Max(fromScale, toScale);
		float minScale = Mathf.Min(fromScale, toScale);

		float stepFrom = fromPos > toPos ? 1 : 0;
		float stepTo = fromPos > toPos ? 0 : 1; // (stepFrom + 1) % 2



		float scaleTime = 0.5f;
		float step = 0; //non-smoothed
    	float rate = 1/scaleTime; //amount to increase non-smooth step by
    	float smoothStep = 0; //smooth step this time
    	float posStep = stepFrom; //smooth step last time


    	while(step < 1f) { // until we're done
			step += Time.deltaTime * rate; //increase the step
			smoothStep = Mathf.SmoothStep(stepFrom, stepTo, step); //get the smooth step

			
			wall.position = new Vector3(wall.position.x, maxPos*posStep+minPos, wall.position.z);
			float sh  = maxPos*posStep/maxPos;
			wall.localScale = new Vector3(wallSize, minScale+sh*(maxScale-minScale), 1f);

			posStep = smoothStep; //store the smooth step
			yield return null;
    	}
		wall.position = new Vector3(wall.position.x, toPos, wall.position.z);
		wall.localScale = new Vector3(wallSize, toScale, 1f);
		ended[ind] = true;
		yield return true;
	}
}
