﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

public class CardObject : MonoBehaviour , IDragHandler, IBeginDragHandler, IEndDragHandler{

	public int charIndex;
	private Vector2 position;

	Transform parentToReturnTo = null;
	Vector2 lastPos = Vector2.zero;



	
    public void OnDrag(PointerEventData eventData)
    {
        this.transform.position = eventData.position;
		if(Vector3.Distance(this.transform.position, lastPos) > 400 && this.transform.localScale.x > 0.2f)
			this.transform.localScale = Vector3.Lerp(this.transform.localScale, Vector3.zero, Time.deltaTime);

	}


    public void OnBeginDrag(PointerEventData eventData)
    {
		parentToReturnTo = this.transform.parent;
        this.transform.SetParent(this.transform.parent.parent.parent.parent);
		lastPos = this.transform.position;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
		if(Vector3.Distance(this.transform.position, lastPos) < 400){
			this.transform.localScale = Vector3.one;
			this.transform.SetParent(parentToReturnTo);
			this.transform.position = lastPos;
		}else{
			Vector3 mousePos = eventData.position;
			mousePos.z = (1-this.transform.localScale.z)*30;
			CharactersController.Instance.SpawnCharacter(charIndex, Camera.main.ScreenToWorldPoint(mousePos));
			Destroy(this.gameObject);
		}
		
		

    }

}
