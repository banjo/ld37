﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Linq;

public class ScoreManager : MonoBehaviour {

	public GameObject text;
	public GameObject button;
	List<Character> characters;
	string results;
	// Use this for initialization
	void Awake () {
		Time.timeScale = 1f;

		results = PlayerPrefs.GetString("Results");

		characters = CharactersController.Instance.characterQueue.Where(x=>x.Active).ToList();
		foreach(Transform child in CharactersController.Instance.transform) Destroy(child.gameObject);
		
		float offset = 10f;
		for(int i=0; i < characters.Count; i++){
			GameObject g = Instantiate(characters[i].prefab, new Vector3(i*offset, 5f, 0), characters[i].prefab.transform.rotation) as GameObject;
			g.transform.GetChild(0).gameObject.SetActive(true);
			g.GetComponent<CharacterBehaviour>().enabled = false;
			g.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeRotation |
														RigidbodyConstraints.FreezePositionX |
														RigidbodyConstraints.FreezePositionZ;
		}
		
	}
	
	// Update is called once per frame
	void Update () {
		if(Camera.main.transform.position.x < 10 * (characters.Count+1))
			Camera.main.transform.Translate(Vector3.right/7);
		else if(!text.activeInHierarchy && !button.activeInHierarchy) {
			text.SetActive(true);
			button.SetActive(true);
			text.GetComponent<Text>().text = results;
		}
	}
}
